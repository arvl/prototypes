import java.util.Scanner;

public class revmtrix {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a number: ");

        int m = input.nextInt();
        int m_size = m * m;
        int m_ctr = 0;

        System.out.print("[");
        for (int i = m_size; i > 0; i--) {
            int temp = i;
            System.out.print(" " + temp);
            m_ctr++;
            if (m_ctr == m) {
                System.out.print(" ]\n");
                m_ctr = 0;
                if ( temp != 1 ) { 
                    System.out.print("[");
                }
            }
        }
    }
}
