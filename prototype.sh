#! /bin/sh

DB_PASSWD="idk"
DB_NAME="projectDB"
TB_NAME="itemsTB"
SESSION_TYPE="anon"

# View function
browseItems() {
    printf "%s\n" "Browsing this list of items..."
    mysql -u root -p"$DB_PASSWD" -D "$DB_NAME" -e "select * from $TB_NAME;"
}

quitProgram() {
    exit 0
}

createItem() {
    printf "%s\n" "Please fill in the following entries."
    printf "%s" "  Item ID: "
    read -r item_id
    printf "%s" "  Item name: "
    read -r item_name
    printf "%s" "  Item description: "
    read -r item_description
    mysql -u root -p"$DB_PASSWD" -D "$DB_NAME" -e "insert into $TB_NAME values ($item_id, '$item_name', '$item_description');"
    unset item_id
    unset item_name
    unset item_description
}

removeItem() {
    printf "%s" "Please enter the item ID: "
    read -r item_id
    mysql -u root -p"$DB_PASSWD" -D "$DB_NAME" -e "delete from $TB_NAME where id = $item_id;"
    unset item_id
}

updateItem() {
    printf "%s" "Enter the item ID of the entry to be updated: "
    read -r item_id
    printf "%s\n" "  1) name "
    printf "%s\n" "  2) description "
    printf "%s" "Which property to update? "
    read -r update_property
    case "$update_property" in
        1)  update_property="Name"
            printf "%s" "  New value: "
            read -r new_value
            mysql -u root -p"$DB_PASSWD" -D "$DB_NAME" -e "update $TB_NAME set $update_property = '$new_value' where ID = $item_id;"
            ;;
        2)  update_property="Description"
            printf "%s" "  New value: "
            read -r new_value
            mysql -u root -p"$DB_PASSWD" -D "$DB_NAME" -e "update $TB_NAME set $update_property = '$new_value' where ID = $item_id;"
            ;;
    esac
    unset item_id
    unset update_property
    unset new_value
}

switchAnon() {
    SESSION_TYPE=anon
    printf "%s\n" "You are now logged in as $SESSION_TYPE."
}

anonPrompt() {
    printf "%s" "Command (? for help): "
    read -r command
    printf "%s\n" ""
    case "$command" in
        l)  browseItems
            ;;
        s)  switchAdmin;
            ;;
        q)  quitProgram
            ;;
        ?)  cat << EOF
l       List items in the databse.
s       Switch to administrator user.
q       Quit the program.
?       Show this help menu.
EOF
    esac
    unset command
}

switchAdmin() {
    printf "%s" "Enter admin database password: "
    read -sr password
    case "$password" in
        "$DB_PASSWD")   SESSION_TYPE=admin
                        printf "%s\n" "You are now logged in as $SESSION_TYPE."
                        ;;
        *)              printf "%s\n" "Login failed!"
                        ;;
    esac
}

adminPrompt() {
    printf "%s" "Command (? for help): "
    read -r command
    printf "%s\n" ""
    case "$command" in
        l)  browseItems
            ;;
        c)  createItem
            ;;
        r)  removeItem
            ;;
        u)  updateItem
            ;;
        s)  switchAnon;
            ;;
        q)  quitProgram
            ;;
        ?)  cat << EOF
l       List items on the databse.
c       Create items on the database.
r       Remove items on the database.
u       Update items on the database.
s       Switch to anonymous user.
q       Quit the program.
?       Show this help menu.
EOF
    esac
    unset command
}

mainPrompt() {
    while true; do
        case "$SESSION_TYPE" in
            anon)   anonPrompt
                    ;;
            admin)  adminPrompt
                    ;;
        esac
    done
}

main() {
    printf "%s\n\n" "Automatically logged in as $SESSION_TYPE."
    browseItems
    mainPrompt
}

main
