import java.util.Scanner;

public class normtrix {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a number: ");

        int m = input.nextInt();
        int m_size = m * m;
        int m_ctr = 0;

        System.out.print("[");
        for (int i = 0; i < m_size; i++) {
            int temp = i + 1;
            System.out.print(" " + temp);
            m_ctr++;
            if (m_ctr == m) {
                System.out.print(" ]\n");
                m_ctr = 0;
                if ( temp != m_size ) { 
                    System.out.print("[");
                }
            }
        }
    }
}
