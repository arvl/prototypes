#! /bin/sh

DB_FILE="hahayes.db"
DB_PATH="$HOME/.config/$(basename $0)/$DB_FILE"

set -e

added_keys=0
ignored_keys=0

checkDB() {
    if [ -e "$DB_PATH" ]; then
        if [ -d "$DB_PATH" ]; then
            printf "$(basename $0): %s\n" "Database path is a directory."
            quitMsg
        fi
    else
        printf "$(basename $0): %s\n" "Database file does not exist."
        createDB
    fi
}

createDB() {
    while true; do
        printf "$(basename $0): %s " "Create database? [y/n]"
        read -r userin
        case "$userin" in
            [Yy])
                mkdir -p "$(dirname $DB_PATH)"
                sqlite3 "$DB_PATH" << EOF
create table KeyStorageHigh    (KeyNumber integer primary key);
create table KeyStorageMiddle  (KeyNumber integer primary key);
create table KeyStorageLow     (KeyNumber integer primary key);
.exit
EOF
                ;;
            [Nn])
                quitMsg
                ;;
            *)
                continue
                ;;
        esac
        break
    done
}

quitMsg() {
    printf "$(basename $0): %s\n" "Goodbye."
    exit 0;
}

addKeyHigh() {
    printf "$(basename $0): %s " "Adding key '$1' to database..."
    if sqlite3 "$DB_PATH" "insert into KeyStorageHigh values ($1)" 2> /dev/null; then
        printf "%s\n" "OK"
        added_keys=$((added_keys + 1))
    else
        printf "%s\n" "FAILED"
    fi
}

addKeyMiddle() {
    printf "$(basename $0): %s " "Adding key '$1' to database..."
    if sqlite3 "$DB_PATH" "insert into KeyStorageMiddle values ($1)" 2> /dev/null; then
        printf "%s\n" "OK"
        added_keys=$((added_keys + 1))
    else
        printf "%s\n" "FAILED"
    fi
}

addKeyLow() {
    printf "$(basename $0): %s " "Adding key '$1' to database..."
    if sqlite3 "$DB_PATH" "insert into KeyStorageLow values ($1)" 2> /dev/null; then
        printf "%s\n" "OK"
        added_keys=$((added_keys + 1))
    else
        printf "%s\n" "FAILED"
    fi
}

ignoreKey() {
    printf "$(basename $0): %s\n" "Ignoring key '$1'"
    ignored_keys=$((ignored_keys + 1))
}

showKeysHigh() {
    printf " %s " "  HIGH:";
    set -- $(sqlite3 "$DB_PATH" "select * from KeyStorageHigh")
    while [ $# -gt 0 ]; do
        printf "%s" "$1"
        if [ $# != 1 ]; then
            printf "%s" " "
        fi
        shift
    done
    printf "%s\n" ""
}

showKeysMiddle() {
    printf " %s " "MIDDLE:";
    set -- $(sqlite3 "$DB_PATH" "select * from KeyStorageMiddle")
    while [ $# -gt 0 ]; do
        printf "%s" "$1"
        if [ $# != 1 ]; then
            printf "%s" " "
        fi
        shift
    done
    printf "%s\n" ""
}

showKeysLow() {
    printf " %s " "   LOW:";
    set -- $(sqlite3 "$DB_PATH" "select * from KeyStorageLow")
    while [ $# -gt 0 ]; do
        printf "%s" "$1"
        if [ $# != 1 ]; then
            printf "%s" " "
        fi
        shift
    done
    printf "%s\n" ""
}

main() {
    checkDB

    case "$1" in
        --list-keys|-l):
            printf "$(basename $0): %s\n" "Summarizing...";
            showKeysHigh
            showKeysMiddle
            showKeysLow
            exit 0
            ;;
        --help|-h):
            cat << EOF
Usage: $(basename $0) [options]

Options:
    -i, --interactive  Add keys in interactive mode.
    -l, --list-keys    List all the keys in storage.
    -h, --help         Show this help menu.
EOF
            exit 0
    esac

    printf "$(basename $0): %s " "Enter the serial keys:"
    read -r serial_keys

    IFS=","
    set -- $serial_keys
    printf "$(basename $0): %s\n" ""
    printf "$(basename $0): %s\n" "Processing..."
    while [ $# -gt 0 ]; do
        [ -z "$1" ] && shift && continue
        key="$1"
        case $key in
            [0-9]|[1-2][0-9]|3[0-2])
                addKeyLow "$key"
                ;;
            [3-5][0-9]|6[0-6])
                addKeyMiddle "$key"
                ;;
            6[7-9]|[7-9][0-9]|100)
                addKeyHigh "$key"
                ;;
            *)
                ignoreKey "$key"
                ;;
        esac
        shift
    done
    printf "$(basename $0): %s\n" "Finished."

    printf "$(basename $0): %s\n" ""
    printf "$(basename $0): %s\n" "Summarizing..."
    printf "$(basename $0): %s\n" "Total keys ADDED:    $added_keys"
    printf "$(basename $0): %s\n" "Total keys IGNORED:  $ignored_keys"
    quitMsg
}

main "$@"
